<?php

namespace Local\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Local\RolesBundle\Entity\Projects;
use Local\RolesBundle\Repository\ProjectsRepository;
use Local\RolesBundle\Form\ProjectsType;
use Symfony\Component\Security\Core\User\UserInterface;

class ProjectsController extends Controller
{
    /**
     * @Route("/client/projects", name="client_projects_show")
     */
    public function showAction(Request $request, UserInterface $member)
    {
        $userid   = $member->getId();

        $em = $this->getDoctrine()->getManager();
        $projects = $em->getRepository('LocalRolesBundle:Projects')->GetProjectsByUserId($userid);

        if (!$projects)
        {
            $projects = array();
            //throw $this->createNotFoundException('ProductController: Unable to find Projects entity.');
        }

        return $this->render('@LocalClient/Projects/show.html.twig', array('user' => $member, 'projects' => $projects));
    }

    /**
     * @Route("/client/projects/edit/{id}", defaults={"id": "1"}, name="client_projects_edit")
     */
    public function editAction(Request $request, $id)
    {
        $project = $this->getDoctrine()
            ->getRepository('LocalRolesBundle:Projects')
            ->find($id);

        $now = new \DateTime('now');

        $project->setTitle($project->getTitle());
        $project->setDescription($project->getDescription());
        $project->setCreatedAt($project->getCreatedAt());
        $project->setUpdatedAt($now);

        $form = $this->createForm(ProjectsType::class, $project);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid() )
        {
            $title          = $form['title']->getData();
            $description    = $form['description']->getData();
            $createdAt      = $form['createdAt']->getData();

            $em = $this->getDoctrine()->getManager();
            $project = $em->getRepository('LocalRolesBundle:Projects')->find($id);

            $project->setTitle($title);
            $project->setDescription($description);
            $project->setCreatedAt($createdAt);
            $project->setUpdatedAt($now);

            $em->flush();

            $this->addFlash('notice', 'Проект обновлен');

            return $this->redirectToRoute('client_projects_show');
        }

        return $this->render('@LocalClient/Projects/edit.html.twig', array('project' => $project, 'form' => $form->createView()));
    }

    /**
     * @Route("/client/projects/details/{id}", name="client_projects_details")
     */
    public function detailsAction($id)
    {
        $project = $this->getDoctrine()
            ->getRepository('LocalRolesBundle:Projects')
            ->find($id);

        return $this->render('@LocalClient/Projects/detail.html.twig', array('project' => $project));
    }
}
