<?php

namespace Local\RolesBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Roles
 *
 * @ORM\Table(name="roles")
 * @ORM\Entity(repositoryClass="Local\RolesBundle\Repository\RolesRepository")
 */
class Roles implements \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=500)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=500)
     */
    private $role;

    /**
     * @var int
     *
     * @ORM\Column(name="permission", type="string", length=255)
     */
    private $permission;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Roles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Roles
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @ORM\OneToMany(targetEntity="Local\RolesBundle\Entity\User", mappedBy="roles")
     */
    private $user;

    /**
     * @var Projects
     * @ORM\ManyToMany(targetEntity="Local\RolesBundle\Entity\Projects", inversedBy="roles")
     * @ORM\JoinTable(name="roles_projects")
     */
    private $projects;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
        $this->projects = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \Local\RolesBundle\Entity\User $user
     *
     * @return Roles
     */
    public function addUser(\Local\RolesBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Local\RolesBundle\Entity\User $user
     */
    public function removeUser(\Local\RolesBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add project
     *
     * @param \Local\RolesBundle\Entity\Projects $project
     *
     * @return Roles
     */
    public function addProject(\Local\RolesBundle\Entity\Projects $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param \Local\RolesBundle\Entity\Projects $project
     */
    public function removeProject(\Local\RolesBundle\Entity\Projects $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->name,
            $this->role,
            $this->permission,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->name,
            $this->role,
            $this->permission,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);//, ['allowed_classes' => false]);
    }

    /**
     * Set permission
     *
     * @param string $permission
     *
     * @return Roles
     */
    public function setPermission($permission)
    {
        $this->permission = $permission;

        return $this;
    }

    /**
     * Get permission
     *
     * @return string
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * Set user
     *
     * @param array $user
     *
     * @return Roles
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
