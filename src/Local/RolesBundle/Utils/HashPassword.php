<?php
namespace Local\RolesBundle\Utils;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Local\RolesBundle\Entity\User;

/**
 * Class HashPassword
 * @package Local\RolesBundle\Utils
 */
class HashPassword
{
    /**
     * @var encoder
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param User $user
     */
    public function encodePassword(User $user)
    {
        $password = $this->encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($password);
    }
}