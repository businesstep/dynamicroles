<?php

namespace Local\RolesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Local\RolesBundle\Entity\User;
use Local\RolesBundle\Form\UserType;

class UserController extends Controller
{
    /**
     * @Route("/user", name="user_list")
     * @IsGranted ("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function listAction(Request $request)
    {
        $users = $this->getDoctrine()
            ->getRepository('LocalRolesBundle:User')
            ->findAll();

        return $this->render('@LocalRoles/User/list.html.twig', array('users' => $users));
    }

    /**
     * @Route("/user/create", name="user_create")
     *
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function createAction(Request $request)
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid() )
        {
            $username   = $form['username']->getData();
            $email      = $form['email']->getData();
            $password   = $form['password']->getData();
            $roles      = $form['roles']->getData();

            $user->setUsername($username);
            $user->setEmail($email);
            $user->setRoles($roles);
            $user->setPassword($password);

            $encoder    = $this->get('utils.hash_password_encoder');
            $encoder->encodePassword($user);

            $em         = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            $this->addFlash('notice', 'Пользователь создан');

            return $this->redirectToRoute('user_list');
        }

        return $this->render('@LocalRoles/User/create.html.twig', array('user'=>$user, 'form' => $form->createView()));
    }

    /**
     * @Route("/user/edit/{id}", defaults={"id": "1"}, name="user_edit")
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function editAction(Request $request, $id)
    {
        $user = $this->getDoctrine()
            ->getRepository('LocalRolesBundle:User')
            ->find($id);

        //$user->setUsername($user->getUsername());
        //$user->setEmail($user->getEmail());
        //$user->setPassword($user->getPassword());

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid() )
        {
            $username   = $form['username']->getData();
            $email      = $form['email']->getData();
            $password   = $form['password']->getData();

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('LocalRolesBundle:User')->find($id);

            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPassword($password);

            $encoder = $this->get('utils.hash_password_encoder');
            $encoder->encodePassword($user);

            $em->flush();

            $this->addFlash('notice', 'Пользователь обновлен');

            return $this->redirectToRoute('user_list');
        }

        return $this->render('@LocalRoles/User/edit.html.twig', array('user' => $user, 'form' => $form->createView()));
    }

    /**
     * @Route("/user/details/{id}", name="user_details")
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function detailsAction($id)
    {
        $user = $this->getDoctrine()
            ->getRepository('LocalRolesBundle:User')
            ->find($id);

        return $this->render('@LocalRoles/User/detail.html.twig', array('user' => $user));
    }

    /**
     * @Route("/user/delete/{id}", name="user_delete")
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('LocalRolesBundle:User')->find($id);

        $em->remove($user);
        $em->flush();

        $this->addFlash('notice', 'Пользователь удален');

        return $this->redirectToRoute('user_list');
    }
}
