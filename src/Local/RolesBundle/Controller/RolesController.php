<?php

namespace Local\RolesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Local\RolesBundle\Entity\Roles;
use Local\RolesBundle\Form\RolesType;

class RolesController extends Controller
{
    /**
     * @Route("/roles", name="roles_list")
     * @IsGranted ("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function listAction(Request $request)
    {
        $roles = $this->getDoctrine()
            ->getRepository('LocalRolesBundle:Roles')
            ->findAll();

        return $this->render('@LocalRoles/Roles/list.html.twig', array('roles' => $roles));
    }

    /**
     * @Route("/roles/create", name="roles_create")
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function createAction(Request $request)
    {
        $roles = new Roles();

        $form = $this->createForm(RolesType::class, $roles);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid() )
        {
            $name       = $form['name']->getData();
            $role       = $form['role']->getData();
            $permission = $form['permission']->getData();

            $roles->setName($name);
            $roles->setRole($role);
            $roles->setPermission($permission);

            $em = $this->getDoctrine()->getManager();
            $em->persist($roles);
            $em->flush();

            $this->addFlash('notice', 'Роль создана');

            return $this->redirectToRoute('roles_list');
        }

        return $this->render('@LocalRoles/Roles/create.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/roles/edit/{id}", defaults={"id": "1"}, name="roles_edit")
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function editAction(Request $request, $id)
    {
        $roles = $this->getDoctrine()
            ->getRepository('LocalRolesBundle:Roles')
            ->find($id);

        $roles->setName($roles->getName());
        $roles->setRole($roles->getRole());
        $roles->setPermission($roles->getPermission());

        $form = $this->createForm(RolesType::class, $roles);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid() )
        {
            $name       = $form['name']->getData();
            $role       = $form['role']->getData();
            $permission = $form['permission']->getData();

            $em = $this->getDoctrine()->getManager();
            $roles = $em->getRepository('LocalRolesBundle:Roles')->find($id);

            $roles->setName($name);
            $roles->setRole($role);
            $roles->setPermission($permission);

            $em->flush();

            $this->addFlash('notice', 'Роль обновлена');

            return $this->redirectToRoute('roles_list');
        }

        return $this->render('@LocalRoles/Roles/edit.html.twig', array('roles' => $roles, 'form' => $form->createView()));
    }

    /**
     * @Route("/roles/delete/{id}", name="roles_delete")
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function deleteAction(Request $request, $id)
    {
        $em     = $this->getDoctrine()->getManager();
        $roles  = $em->getRepository('LocalRolesBundle:Roles')->find($id);

        $em->remove($roles);
        $em->flush();

        $this->addFlash('notice', 'Роль удалена');

        return $this->redirectToRoute('roles_list');
    }
}