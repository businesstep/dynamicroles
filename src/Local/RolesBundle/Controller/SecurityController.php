<?php

namespace Local\RolesBundle\Controller;

use Local\RolesBundle\Entity\User;
use Local\RolesBundle\Form\UserType;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Local\RolesBundle\Form\AuthForm;
//use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * @Route("/", name="security_auth")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function authorizationAction(Request $request)
    {
        $authUtils      = $this->get('security.authentication_utils');
        $errors         = $authUtils->getLastAuthenticationError();
        $lastUserName   = $authUtils->getLastUsername();

        $form = $this->createForm(AuthForm::class, array('_username'=>$lastUserName, 'action' => $this->generateUrl('security_success')));
        $form->handleRequest($request);

        return $this->render('@LocalRoles/Security/authorization.html.twig', array(
            'form'          => $form->createView(),
            'errors'        => $errors,
        ));
    }

    /**
     * @Route("/logout", name="security_logout")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function logoutAction()
    {
        return $this->render('@LocalRoles/Security/authorization.html.twig');
    }

    /**
     * @Route("/success", name="security_success")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function successAction(Request $request, UserInterface $member)
    {
        $userid   = $member->getId();

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            return $this->redirectToRoute('projects_list');
        }

        $em = $this->getDoctrine()->getManager();

        $projects = $em->getRepository('LocalRolesBundle:Projects')->GetProjectsByUserId($userid);

        if (!$projects)
        {
            $projects = array();
            //throw $this->createNotFoundException('ProductController: Unable to find Projects entity.');
        }

        return $this->redirectToRoute('client_projects_show');
        //return $this->render('@LocalClient/Projects/show.html.twig', array('user' => $member, 'projects' => $projects));
    }
}