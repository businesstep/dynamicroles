<?php

namespace Local\RolesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Local\RolesBundle\Entity\Projects;
use Local\RolesBundle\Repository\ProjectsRepository;
use Local\RolesBundle\Form\ProjectsType;

class ProjectsController extends Controller
{
    /**
     * @Route("/projects", name="projects_list")
     * @IsGranted ("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function listAction(Request $request)
    {
        $projects = $this->getDoctrine()
            ->getRepository('LocalRolesBundle:Projects')
            ->findAll();

        return $this->render('@LocalRoles/Projects/list.html.twig', array('projects' => $projects));
    }

    /**
     * @Route("/projects/create", name="projects_create")
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function createAction(Request $request)
    {
        $project = new Projects();

        $form = $this->createForm(ProjectsType::class, $project);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid() )
        {
            $title          = $form['title']->getData();
            $description    = $form['description']->getData();
            $createdAt      = $form['createdAt']->getData();

            $now = new \DateTime('now');

            $project->setTitle($title);
            $project->setDescription($description);
            $project->setCreatedAt($createdAt);
            $project->setUpdatedAt($now);

            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            $this->addFlash('notice', 'Проект создан');

            return $this->redirectToRoute('projects_list');
        }

        return $this->render('@LocalRoles/Projects/create.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/projects/edit/{id}", defaults={"id": "1"}, name="projects_edit")
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function editAction(Request $request, $id)
    {
        $project = $this->getDoctrine()
            ->getRepository('LocalRolesBundle:Projects')
            ->find($id);

        $now = new \DateTime('now');

        $project->setTitle($project->getTitle());
        $project->setDescription($project->getDescription());
        $project->setCreatedAt($project->getCreatedAt());
        $project->setUpdatedAt($now);

        $form = $this->createForm(ProjectsType::class, $project);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid() )
        {
            $title          = $form['title']->getData();
            $description    = $form['description']->getData();
            $createdAt      = $form['createdAt']->getData();

            $em = $this->getDoctrine()->getManager();
            $project = $em->getRepository('LocalRolesBundle:Projects')->find($id);

            $project->setTitle($title);
            $project->setDescription($description);
            $project->setCreatedAt($createdAt);
            $project->setUpdatedAt($now);

            $em->flush();

            $this->addFlash('notice', 'Проект обновлен');

            return $this->redirectToRoute('projects_list');
        }

        return $this->render('@LocalRoles/Projects/edit.html.twig', array('project' => $project, 'form' => $form->createView()));
    }

    /**
     * @Route("/projects/details/{id}", name="projects_details")
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function detailsAction($id)
    {
        $project = $this->getDoctrine()
            ->getRepository('LocalRolesBundle:Projects')
            ->find($id);

        return $this->render('@LocalRoles/Projects/detail.html.twig', array('project' => $project));
    }

    /**
     * @Route("/projects/delete/{id}", name="projects_delete")
     * @IsGranted("ROLE_ADMIN", statusCode=404, message="not found")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository('LocalRolesBundle:Projects')->find($id);

        $em->remove($project);
        $em->flush();

        $this->addFlash('notice', 'Проект удален');

        return $this->redirectToRoute('projects_list');
    }
}
