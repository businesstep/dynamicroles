<?php
namespace Local\RolesBundle\Form;

use Local\RolesBundle\Entity\Projects;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',      TextType::class, array('label' => 'Название', 'attr' => array('required'=>'required', 'autofocus'=>'autofocus', 'placeholder'=>'название', 'class' => 'form-control')))
            ->add('description',TextAreaType::class, array('label'=> 'Описание', 'attr' => array('required'=>'required', 'placeholder'=>'описание', 'class' => 'form-control')))
            ->add('createdAt',  DateType::class, array('label'=> 'Дата создания', 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'attr' => array('class' => 'form-control')));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        //parent::configureOptions($resolver); // TODO: Change the autogenerated stub
        $resolver->setDefaults(array('data_class'=>Projects::class));
    }
}