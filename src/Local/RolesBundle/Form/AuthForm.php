<?php
namespace Local\RolesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class AuthForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username',   TextType::class, array('label'=> false, 'attr' => array('required'=>'required', 'placeholder'=>'e-mail', 'class' => 'form-control form-group')))
            ->add('_password',   PasswordType::class, array('label'=> false, 'attr' => array('required'=>'required', 'value'=>'', 'placeholder'=>'пароль', 'class' => 'form-control form-group')));
    }
}