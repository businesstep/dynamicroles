<?php

namespace Local\RolesBundle\Security;

use Doctrine\ORM\EntityManagerInterface;
use Local\RolesBundle\Form\AuthForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    private $em;
    private $encoder;
    private $router;
    private $formFactory;

    public function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $em, RouterInterface $router, UserPasswordEncoderInterface $encoder)
    {
        $this->formFactory  = $formFactory;
        $this->em           = $em;
        $this->router       = $router;
        $this->encoder      = $encoder;
    }

    protected function getLoginUrl()
    {
        return $this->router->generate('security_auth');
    }

    public function getCredentials(Request $request)
    {
        $isLoginSubmit = $request->getPathInfo() == '/' && $request->isMethod('POST');

        if(!$isLoginSubmit)
        {
            return null;
        }

        $form = $this->formFactory->create(AuthForm::class);
        $form->handleRequest($request);
        $data = $form->getData();

        return $data;
    }

    public function getUser($credentials, UserProviderInterface $provider)
    {
        $username = $credentials['_username'];

        return $this->em->getRepository('LocalRolesBundle:User')->findOneBy(array('email' => $username));
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['_password'];

        if($this->encoder->isPasswordValid($user, $password))
        {
            return true;
        }

        return false;
    }

    protected function getDefaultSuccessRedirectUrl()
    {
        return $this->router->generate('security_success');
    }
}