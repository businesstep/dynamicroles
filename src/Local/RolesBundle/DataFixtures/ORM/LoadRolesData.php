<?php

namespace Local\RolesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Local\RolesBundle\Entity\Roles;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class LoadRolesData
 * @package Local\RolesBundle\DataFixtures\ORM
 */
class LoadRolesData implements FixtureInterface
{
    /**
     * Load data fixtures
     */
    public function load(ObjectManager $manager)
    {
        $roles   = new Roles;

        $roles->setName('user');
        $roles->setRole('ROLE_USER');
        $roles->setPermission('view');

        $manager->persist($roles);
        $manager->flush();
    }
}
