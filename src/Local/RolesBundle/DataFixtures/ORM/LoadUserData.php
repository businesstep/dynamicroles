<?php

namespace Local\RolesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Local\RolesBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class LoadUserData
 * @package Local\RolesBundle\DataFixtures\ORM
 */
class LoadUserData implements FixtureInterface
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * Load data fixtures
     */
    public function load(ObjectManager $manager)
    {
        $user   = new User();

        $user->setUsername('admin');
        $user->setEmail('test@localhost');

        $password   = $this->encoder->encodePassword($user, 'adminadmin');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();
    }
}
